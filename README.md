# less1.9

magic with skukozh

```sh
adduser deer
```
![](images/1.png)

```sh
echo "deer ALL = (root) NOPASSWD: /usr/local/bin/skukozh" > /etc/sudoers.d/deer
visudo --check /etc/sudoers.d/deer
chmod 550 /usr/local/bin/skukozh
ls -la /usr/local/bin/skukozh
su - deer
cp /bin/ls ~/ls
~/ls /
sudo skukozh ~/ls
~/ls /
```

![](images/2.png)